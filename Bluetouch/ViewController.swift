//
//  ViewController.swift
//  Bluetouch
//
//  Created by Mykola Kibysh on 6/20/18.
//  Copyright © 2018 NickKibish. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UITableViewController {
    var pereferals: [CBPeripheral] = []
    
    lazy var centralManager = {
        return CBCentralManager(delegate: self, queue: DispatchQueue.global(qos: .default))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pereferals.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let p = pereferals[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = p.name
        return cell!
    }
}

extension ViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        self.pereferals.append(peripheral)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
